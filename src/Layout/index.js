import React, { useState } from "react";
import { FiMoreVertical, FiBell, FiX } from "react-icons/fi";
import SideNav from "./sideNav";
import {
  Content,
  HeaderLeft,
  HeaderRight,
  HeaderMobile,
  Main,
  Img,
  Modal,
} from "./styled";

const Layout = (props) => {
  const [active, setActive] = useState(false);
  const { children } = props;
  const width = window.innerWidth;

  return (
    <>
      {width <= 800 ? (
        <Main>
          <HeaderMobile>
            <FiBell size="25px" />
            <FiMoreVertical size="25px" onClick={() => setActive(!active)} />
          </HeaderMobile>
          <Content>{children}</Content>
          <Modal active={active}>
            <FiX
              className="fix"
              size="25px"
              onClick={() => setActive(!active)}
            />
            <SideNav />
          </Modal>
        </Main>
      ) : (
        <Main>
          <HeaderLeft>
            <Img src="assets/logo.png" alt="" />
          </HeaderLeft>
          <HeaderRight>
            <FiBell className="bell" size="25px" style={{ right: 0 }} />
          </HeaderRight>
          <SideNav />
          <Content> {children} </Content>
        </Main>
      )}
    </>
  );
};
export default Layout;
