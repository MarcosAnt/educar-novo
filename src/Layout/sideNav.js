import React, { useState } from "react";
import Lotacoes from "../components/Navigator/Lotacoes";
import Suporte from "../components/Navigator/Suport";
import Perfil from "../components/Perfil";
import { SideNav } from "./styled";

const Layout = () => {
  const [selected, setSelected] = useState(0);
  const [list] = useState([
    "SECRETARIA MUNICIPAL DE EDUCACAO",
    "APAE - ASSOCIAÇÃO DE PAIS E MESTRES",
    "CARLOS",
  ]);
  return (
    <SideNav>
      <Perfil />
      <Lotacoes active={list} selected={selected} setSelected={setSelected} />
      <Suporte />
    </SideNav>
  );
};
export default Layout;
