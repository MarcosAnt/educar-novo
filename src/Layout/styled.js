import styled from "styled-components";

export const Img = styled.img`
  height: 40px;
  margin-left: 8%;
`;
export const Main = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;

  @media (max-width: 800px) {
    grid-template-columns: 1fr;
  }
`;
export const Content = styled.div`
  padding: 2vw;
  box-shadow: 0px 4px 25px rgba(0, 0, 0, 0.13);
  @media (max-width: 800px) {
    height: 100vh;
  }
`;
export const HeaderLeft = styled.div`
  height: 8vh;
  display: flex;
  align-items: center;
  background-color: #f4f4f4;
  border-bottom: 2px solid rgb(217, 214, 214, 0.5);
  @media (max-width: 800px) {
    display: none;
  }
`;
export const HeaderRight = styled.div`
  height: 8vh;
  display: flex;
  align-items: center;
  box-shadow: -2px -1px 25px rgba(0, 0, 0, 0.13);
  border-bottom: 2px solid rgb(217, 214, 214, 0.5);
  .bell {
    z-index: 999;
    margin-right: 50px;
    position: absolute;
  }
  @media (max-width: 800px) {
    display: none;
  }
`;

export const SideNav = styled.div`
  height: 91vh;
  padding: 8%;
  background-color: #f4f4f4;
  @media (max-width: 800px) {
    width: 90vw;
    height: 100%;
    z-index: 99;
    animation: menu 0.6s;
    @keyframes menu {
      0% {
        width: 0vw;
      }
      100% {
        width: 90vw;
      }
    }
  }
`;

export const Modal = styled.div`
  position: fixed;
  justify-content: flex-end;
  z-index: 9;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: rgb(0, 0, 0, 0.3);
  display: ${(props) => (props.active ? "flex" : "none")};
  .fix {
    justify-self: flex-end;
    z-index: 999;
    right: 20px;
    top: 20px;
    position: absolute;
  }
`;

export const HeaderMobile = styled.div`
  height: 7vh;
  display: flex;
  padding: 10px 25px;
  align-items: center;
  background-color: #fff;
  justify-content: space-between;
`;
