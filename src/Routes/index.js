import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Layout from "../Layout";

import Painel from "../Pages/painel";
import Login from "../Pages/login";

export default function routes() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Painel} />
          <Route exact path="/contato" component={Login} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}
