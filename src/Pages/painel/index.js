import React from "react";
import Cards from "../../components/Cards";
import { Content } from "./styled";

export default function Home() {
  return (
    <Content>
      <Cards />
      <Cards />
      <Cards />
      <Cards />
      <Cards />
      <Cards />
    </Content>
  );
}
