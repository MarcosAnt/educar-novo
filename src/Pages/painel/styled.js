import styled from "styled-components";

export const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  grid-gap: 30px;
  @media (max-width: 800px) {
    padding: 5%;
    grid-template-columns: repeat(2, 1fr);
  }
  @media (min-width: 801px) and (max-width: 1300px) {
    grid-template-columns: repeat(4, 1fr);
  }
  @media (min-width: 1301px) and (max-width: 1500px) {
    grid-template-columns: repeat(5, 1fr);
  }
`;
