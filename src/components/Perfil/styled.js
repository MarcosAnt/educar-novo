import styled from "styled-components";

export const Perfil = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 25px;
`;

export const Name = styled.h4`
  font-weight: 700;
  line-height: 1;
`;

export const Lotacao = styled.h5`
  font-weight: 500;
  color: #9e9e9e;
  margin-bottom: 0px;
`;
