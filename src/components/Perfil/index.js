import React from "react";
import { Perfil, Name, Lotacao } from "./styled";
import { Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";

function index() {
  return (
    <Perfil>
      <Avatar size={55} icon={<UserOutlined />} />
      <div style={{ marginLeft: 15 }}>
        <Name>EMANUELLE LONGO DE OLIVEIRA</Name>
        <Lotacao>SECRETARIA MUNICIPAL DE EDUCACAO</Lotacao>
      </div>
    </Perfil>
  );
}

export default index;
