import styled from "styled-components";

export const Card = styled.div`
  height: 180px;
  width: 166.19px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  box-shadow: 0px 0px 9px rgba(0, 0, 0, 0.21);
  border-radius: 7px;

  @media (max-width: 800px) {
    height: 180px;
    width: 100%;
  }
`;
export const CardIcon = styled.div``;
export const CardName = styled.div`
  height: 30%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #a3cb38;
  border-radius: 0px 0px 7px 7px;
`;
export const Name = styled.h4`
  color: white;
`;
