import React from "react";
import { Card, CardIcon, CardName, Name } from "./styled";

function Cards() {
  return (
    <Card>
      <CardIcon />
      <CardName>
        <Name>NOME CARD</Name>
      </CardName>
    </Card>
  );
}

export default Cards;
