import React from "react";
import { BsBriefcaseFill } from "react-icons/bs";
import { Button } from "antd";
import { Lotacoes, Content, Options } from "./styled";

const locacaoOptions = (props) => {
  return (
    <Lotacoes>
      <Content>
        <BsBriefcaseFill size="18px" style={{ color: "#162433" }} />
        <h3> Selecionar lotação</h3>
      </Content>
      <Options>
        {props.active.map((item, index) => {
          let bg = `${props.selected === index ? "#65c2ab" : ""}`;
          let text = `${props.selected === index ? "#fff" : "#9e9e9e"}`;
          return (
            <Button
              onClick={() => props.setSelected(index)}
              type="text"
              style={{ backgroundColor: bg, color: text }}
            >
              {item}
            </Button>
          );
        })}
      </Options>
    </Lotacoes>
  );
};

export default locacaoOptions;
