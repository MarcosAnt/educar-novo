import React from "react";
import { FaHeadset } from "react-icons/fa";
import { Button } from "antd";
import { Lotacoes, Content, Options } from "./styled";

const locacaoOptions = () => {
  return (
    <Lotacoes>
      <Content>
        <FaHeadset size="18px" style={{ color: "#162433" }} />
        <h3>Suporte</h3>
      </Content>
      <Options>
        <Button type="text">
          {" "}
          <FaHeadset size="13px" /> Chat
        </Button>
      </Options>
    </Lotacoes>
  );
};

export default locacaoOptions;
