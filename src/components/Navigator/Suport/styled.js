import styled from "styled-components";

export const Lotacoes = styled.div``;
export const Content = styled.div`
  display: flex;
  align-items: center;
  h3 {
    color: #737272;
    margin: 10px 15px;
    font-weight: 600;
    font-family: "Roboto", sans-serif;
  }
`;
export const Options = styled.div`
  display: flex;
  flex-direction: column;
  Button {
    background-color: #162433;
    color: #fff;
    font-size: 13px;
    margin-bottom: 10px;
    text-align: left;
  }
`;
